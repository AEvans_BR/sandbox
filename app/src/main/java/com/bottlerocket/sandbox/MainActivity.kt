package com.bottlerocket.sandbox

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.scene_end.*
import kotlinx.android.synthetic.main.simple_item.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = CustomAdapter(genStrings())

    }

    fun genStrings(): List<String> {
        val list = mutableListOf<String>()
        var i = 0;
        while (i < 100) {
            list.add("This is an item")
            i++
        }
        return list.toList()
    }
}


class CustomAdapter(val strings: List<String>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun getItemCount(): Int = strings.size
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.onBind(strings[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.simple_item, parent, false)
        return CustomViewHolder(view)
    }
}

class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(string: String) {
        itemView.text.text = string
    }

}

